#include <iostream>
#include <lua_evaluator.h>

int main(int argc, char *argv[]) {
  LuaEvaluator l;
  if (argc < 3) {
    std::cout << "usage: " << argv[0] << " EXPRESSION TARGET_UNIT\n";
    return 1;
  }
  std::string_view expression = argv[1];
  std::string_view target_unit = argv[2];
  auto result = l.eval(expression, target_unit);
  std::cout << expression << " -> " << result;
  if (target_unit != "_1")
    std::cout << " * " << target_unit;
  std::cout << '\n';
  return 0;
}
