#include <lua_evaluator.h>

#include <array>
#include <iostream>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <stdexcept>

#include <load_physical.h>

LuaEvaluator::LuaEvaluator() : L(luaL_newstate()) {
  if (!L)
    throw std::runtime_error("failed to create Lua state");

  // load needed standard libraries
  const std::array<luaL_Reg, 5> mods{{{LUA_GNAME, luaopen_base},
                                      {LUA_LOADLIBNAME, luaopen_package},
                                      {LUA_TABLIBNAME, luaopen_table},
                                      {LUA_STRLIBNAME, luaopen_string},
                                      {LUA_MATHLIBNAME, luaopen_math}}};
  for (auto &[n, f] : mods) {
    luaL_requiref(L, n, f, 1);
  }

  // load physical units modules
  load_physical(L);

  // clean up the stack
  lua_settop(L, 0);
}

LuaEvaluator::~LuaEvaluator() {
  if (L)
    lua_close(L);
}

double LuaEvaluator::eval(std::string_view expression,
                          std::string_view result_unit) {
  if (expression.empty() || result_unit.empty()) {
    throw std::runtime_error(
        "cannot evaluate an empty expression or use and empty unit");
  }

  // to evaluate the expression we need to assign the result to a variable
  // the "_1" is to make sure pure numerical expressions have a valid dimension
  std::string wrapped_expression("result=_1*(");
  wrapped_expression += expression;
  wrapped_expression += ')';

  // evaluate the expression
  if (luaL_dostring(L, wrapped_expression.c_str()) != LUA_OK) {
    size_t err_size;
    auto err_start = lua_tolstring(L, 1, &err_size);
    std::string err(err_start, err_size);
    throw std::runtime_error("error evaluating expression `" +
                             std::string(expression) + "`: " + err);
  }

  // convert to the requested unit
  if (luaL_dostring(L, ("result=result/(" + std::string{result_unit} +
                        ");result=result:to(_1)")
                           .c_str()) != LUA_OK) {
    size_t err_size;
    auto err_start = lua_tolstring(L, 1, &err_size);
    std::string err(err_start, err_size);
    throw std::runtime_error("error converting to unit `" +
                             std::string(result_unit) + "`: " + err);
  }

  // get the result as a number
  lua_getglobal(L, "result");
  luaL_callmeta(L, -1, "__tonumber");
  auto number = lua_tonumber(L, -1);

  // clear the stack
  lua_settop(L, 0);

  // reset the global name
  lua_pushnil(L);
  lua_setglobal(L, "result");

  return number;
}
