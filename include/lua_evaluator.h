#pragma once
#include <boost/algorithm/string/replace.hpp>
#include <boost/units/io.hpp>
#include <string>
#include <string_view>

struct lua_State;

namespace details {
template <auto unit> std::string unit_repr() {
  std::string unit_repr = to_string(unit);
  if (unit_repr == "dimensionless")
    return "_1";
  unit_repr = boost::replace_all_copy(unit_repr, " ", "*");
  return boost::replace_all_copy(unit_repr, "k(", "1000*(");
}
template <> inline std::string unit_repr<1>() { return "_1"; }
} // namespace details

class LuaEvaluator {
public:
  LuaEvaluator();
  ~LuaEvaluator();
  double eval(std::string_view expression, std::string_view result_unit);

  template <auto unit = 1> auto eval(const std::string_view expression) {
    const auto value = eval(expression, details::unit_repr<unit>());
    return value * unit;
  }

private:
  lua_State *L = nullptr;
};
