#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <lua_evaluator.h>

#include <boost/units/base_units/metric/hour.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>

using namespace boost::units::si;
using Catch::Approx;

TEST_CASE("simple cases") {
  constexpr auto km = kilo * meters;
  constexpr auto h = boost::units::metric::hour_base_unit::unit_type{};

  LuaEvaluator l;
  CHECK(l.eval("10 * m", "mm") == 10000.);
  CHECK(l.eval<meters / second>("(10 * m + 1000 * mm) / min").value() ==
        Approx(0.183333));
  CHECK(l.eval<km>("1 * au").value() == 149597870.700);
  CHECK(l.eval<km / h>("1 * km/h").value() == 1.);
  CHECK(l.eval<metre / second>("1 * mi/h").value() == Approx(0.44704));
}

TEST_CASE("errors") {
  LuaEvaluator l;

  SECTION("invalid conversion") {
    CHECK_THROWS(l.eval("m", "kg"));
    CHECK_THROWS(l.eval<meters>("10 * h"));
  }

  SECTION("invalid operation") {
    CHECK_THROWS(l.eval("1 * m + 1 * s", "_1"));
    CHECK_THROWS(l.eval<>("1 * m + 1 * s"));
  }
}
